# Protos PHP Framework #

Protos is a lightweight PHP framework designed for small and medium developers that focus on speed-of-implementation, extensibility and control. 
It is also very fast! It helps connect with the latest back- and frontend technologies through Composer, Bower and NPM so you will not be lacking features. 

### How to install? ###
In your console type:
composer require protos/build
continue
then run: composer install
when its done type:
php vendor/protos/build/console protos:generate:skeleton skeleton.xml
And this will create your bare skeleton.

Type: php vendor/protos/build/console
If you want to see all build commands available to you.

### Is there a tutorial? ###
For example websites build in protos you may require an example package through your composer.json
Or you may download it from http://bitbucket.org/protosphp