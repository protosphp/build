<?php
namespace Protos\Command;

use \Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Input\InputArgument;

global $router;
$router = new \stdClass();

global $const;
$const = [];

class BuildSkeletonCmd extends Command {
	
	protected $bootstrap = null;
	
	protected function configure(){
		$this->setName('protos:generate:skeleton');
		$this->setDescription('Creates a blank skeleton from your input (e.g. skeleton.xml)');
		$this->setHelp("Creates a bare folder structure for the framework. You can change this structure to suite your own needs.");
		$this->addArgument('filename', InputArgument::REQUIRED, 'Path to a skeleton xml');
		$this->addArgument('router', InputArgument::OPTIONAL, 'Path to a dummy router object (optional)');
	}
	
	public function execute(InputInterface $input, OutputInterface $output){
		global $router, $const;
		$output->writeln([
			'Protos Skeleton Builder',
			'=======================',
			'',
		]);
		
		$path = realpath(__DIR__.'/../../../').'/';
		$pathConfig = "<?php\r\n";
		
		if($routerPath = $input->getArgument('router') && file_exists($path.$routerPath)){
			include $path.$routerPath;
		}else{
			$router->application = 'AcmeApp';
			$router->locale = 'int';
			$router->lang = 'en_US';
			$router->accessgroup = 'default';
			$router->guid = '3f2504e0-4f89-11d3-9a0c-0305e82c3301';
			$router->service = 'www';
			$router->api = '1.0';
			$router->controller = 'home';
			$router->action = 'index';
			$router->theme = 'default';
			$router->template = 'default';
			$router->layout = 'html';
			$router->view = 'index';
			$router->classname = 'Portfolio';
		}
		$fileName = $input->getArgument('filename');
		if(file_exists($path.$fileName)){
			$xml = simplexml_load_file($path.$fileName);
			if(!$constant = (string)$xml->attributes()->as){
				$constant = "_ROOT_";
			}
			$pathConfig .= "define(\"$constant\",\trtrim(realpath(\".\"),\"/\").\"/\");\r\n";
			if($xml->locate){
				foreach($xml->locate as $node){
					if($node instanceof \SimpleXMLElement){
						$this->parseXML($node, '', $output);
					}
				}
			}
		}else{
			$output->writeln($path.$fileName.' NOT found!');
		}
		
		foreach($const as $constant => $path){
			$pathConfig .= "define(\"$constant\",\t_ROOT_.\"$path\");\r\n";
		}
		file_put_contents($this->bootstrap."global.path.php", $pathConfig);
		$output->writeln("write ".$this->bootstrap."global.path.php >> ".number_format(strlen($pathConfig)/1024, 1)."Kb");
	}
	
	private function evaluate($mixedPathExpression){
		global $router;
		$pathBits = explode(".", $mixedPathExpression);
		$expressions = [];
		foreach($pathBits as $bit => $path){
			preg_match_all('%({\$?(.+?)(?:->(.+))?})%im', $path, $result);
			if($result){
				foreach($result[1] as $key => $expression){
					$object = $result[2][$key];
					$value = $result[3][$key];
					//if($$object->$value){
					$expressions[$expression] = $$object->$value;
					//}
				}
			}
		}
		$mixedPathExpression = implode(".", $pathBits);
		foreach($expressions as $expression => $value){
			$mixedPathExpression = str_replace($expression, $value, $mixedPathExpression);
		}
		
		return preg_replace('%[^\w.\-/]%im', '', $mixedPathExpression);
	}
	
	private function parseXML(\SimpleXMLElement $xml, $rootPath, OutputInterface $output){
		global $const;
		if($map = (string)$xml->attributes()->map){
			$map = $this->evaluate($map);
			$rootPath .= $map.'/';
			$output->writeln("mkdir ".trim($rootPath, '/'));
			@mkdir($rootPath);
		}
		if($file = (string)$xml->attributes()->file){
			$file = $this->evaluate($file);
			if(substr($file, 0, 1)!="."){
				if($code = (string)$xml){
					$output->writeln("write ".$rootPath.$file." >> ".number_format(strlen($code), 0)."b");
					file_put_contents($rootPath.$file, $code);
				}else{
					$output->writeln("touch ".$rootPath.$file);
					file_put_contents($rootPath.$file, '');
				}
			}else{
				$file = null;
			}
		}
		if($as = (string)$xml->attributes()->as){
			$const[$as] = $rootPath;
			if($as=="_BOOTSTRAP_"){
				$this->bootstrap = $rootPath;
			}
		}
		if($xml->locate){
			foreach($xml->locate as $node){
				if($node instanceof \SimpleXMLElement){
					$this->parseXML($node, $rootPath, $output);
				}
			}
		}elseif(!$file){
			$output->writeln("touch ".$rootPath.".gitkeep");
			file_put_contents($rootPath.".gitkeep", '');
		}
		if($xml->install){
			foreach($xml->install as $install){
				$branch = (string)$install->attributes()->branch;
				if($remote = (string)$install->attributes()->remote){
					if($branch){
						$output->writeln("install ref $remote:$branch");
					}else{
						$output->writeln("install ref $remote");
					}
				}else{
					if($branch){
						$output->writeln("install branch $branch");
					}
				}
			}
			$output->writeln("The auto-install branch feature function has not yet been implemented.");
		}
		if($xml->download){
			foreach($xml->download as $download){
				if($url = (string)$download->attributes()->url){
					$output->writeln("get $url");
				}
			}
			$output->writeln("The auto-download function has not yet been implemented.");
		}
	}
}
